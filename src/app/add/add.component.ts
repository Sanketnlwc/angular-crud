import { Component, OnInit } from '@angular/core';
import { EmployeeListService } from '../employee-list.service';
import {employee} from '../EmployeeList';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent  {

  constructor(private _employeeListService : EmployeeListService, private router: Router) {  }

   addContact( Name , Salary, Designation)
     {
     	let employeeDetails : employee= { 
         "id" : Math.random(),
			"Name" : Name,
			"Salary" : Salary,
			"Designation" : Designation
				};
     	this._employeeListService.addEmployee(employeeDetails);
     	alert("Contact Added Succesfully");
     	this.router.navigate(['/']);
     }
     cancelAdd(){
     	this.router.navigate(['/']);
    }
}
