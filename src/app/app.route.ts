import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home-component/home-component.component';
import { AddComponent } from './add/add.component';
import { EditComponent } from './edit/edit.component';

export const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', redirectTo :'', component: HomeComponent },
  { path: 'edit/:id', component: EditComponent },
  { path: 'add', component: AddComponent }
];